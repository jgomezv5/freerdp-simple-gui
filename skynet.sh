#!/bin/bash
# Julián Gómez
# Interfaz sencilla para FreeRDC
# 18-05-2017

fundialog=${fundialog=dialog}

pantalla=`$fundialog --stdout --menu "Pantalla:" 0 0 0 1 "Sencilla" 2 "Múltiple"`

archivos=`$fundialog --stdout --menu "Compartir carpeta personal:" 0 0 0 1 "Si" 2 "No"`

servidor=`$fundialog --stdout --title "servidor" --inputbox "Conectar a:" 0 0`

usuario=`$fundialog --stdout --title "usuario" --inputbox "Usuario:" 0 0`

password=`$fundialog --stdout --title "password" --passwordbox "Contraseña:" 0 0`

if(("$pantalla" == "1"));
then

	if(("$archivos" == "1"));
	then
		xfreerdp +compression +home-drive +fonts /sound /f /u:"$usuario" /p:"$password" /v:"$servidor"
	fi

	if(("$archivos" == "2"));
	then
		xfreerdp +compression +fonts /sound /f /u:"$usuario" /p:"$password" /v:"$servidor"
	fi

fi

if(("$pantalla" == "2"));
then

	if(("$archivos" == "1"));
	then
		xfreerdp +compression +home-drive +fonts /sound /multimon /f /u:"$usuario" /p:"$password" /v:"$servidor"
	fi

	if(("$archivos" == "2"));
	then
		xfreerdp +compression +fonts /sound /multimon /f /u:"$usuario" /p:"$password" /v:"$servidor"
	fi
	
fi




